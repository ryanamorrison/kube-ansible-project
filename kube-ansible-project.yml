---
- hosts: localhost
  gather_facts: no

  vars:
     var_ns: mongo-app 
     var_app_setting_1: foo
     var_app_setting_2: baz
     var_secret_name: api-sekret

  tasks:
    - name: import vault vars
      include_vars:
        file: vault.yml
      no_log: True

    - name: ensure namespace
      kubernetes.core.k8s:
        name: "{{ var_ns }}"
        api_version: v1
        kind: Namespace
        state: present

    - name: ensure configmap
      kubernetes.core.k8s:
        namespace: "{{ var_ns }}"
        state: present
        definition:
          apiVersion: v1
          kind: ConfigMap
          metadata:
            name: appconfigmap
          data:
            application-setting-1: "{{ var_app_setting_1 }}"
            application-setting-2: "{{ var_app_setting_2 }}"

    - name: ensure secret using encoding method
      kubernetes.core.k8s:
        namespace: "{{ var_ns }}"
        state: present
        definition:
          apiVersion: v1
          kind: Secret
          metadata:
            name: "{{ var_secret_name }}"
          data:
            apikey: "{{ var_api_key_config }}"
      no_log: True

    - name: ensure storage class
      kubernetes.core.k8s:
        namespace: "{{ var_ns }}"
        state: present
        definition:
          apiVersion: storage.k8s.io/v1
          kind: StorageClass
          metadata:
            name: "ebs-sc-{{ var_ns }}"
          provisioner: ebs.csi.aws.com
          volumeBindingMode: WaitForFirstConsumer
          allowVolumeExpansion: true
          mountOptions:
            - debug

    - name: ensure PVC
      kubernetes.core.k8s:
        namespace: "{{ var_ns }}"
        state: present
        definition:
          apiVersion: v1
          kind: PersistentVolumeClaim
          metadata:
            name: mongo-ebs-claim
          spec:
            accessModes:
              - ReadWriteOnce
            storageClassName: "ebs-sc-{{ var_ns }}"
            resources:
              requests:
                storage: 20Gi

    - name: ensure mongo headless service
      kubernetes.core.k8s:
        namespace: "{{ var_ns }}"
        state: present
        definition:
          apiVersion: v1
          kind: Service
          metadata:
            name: mongo-service
            labels:
              name: mongo
          spec:
            ports:
            - port: 27017
              targetPort: 27017
            clusterIP: None
            selector:
              role: mongodb

    - name: ensure mongo statefulset
      kubernetes.core.k8s:
        namespace: "{{ var_ns }}"
        state: present
        definition:
          apiVersion: apps/v1
          kind: StatefulSet
          metadata:
            name: mongodb
          spec:
            serviceName: mongo-service
            replicas: 3
            selector:
              matchLabels:
                role: mongodb
            template:
              metadata:
                labels:
                  role: mongodb
                  environment: testing
                  replicaset: MongoReplicaSet
              spec:
                terminationGracePeriodSeconds: 10
                containers:
                  - name: mongod-container
                    image: mongo
                    command:
                      - "mondod"
                      - "--bind_ip"
                      - "0.0.0.0"
                      - "--replSet"
                      - "MongoReplicaSet"
                    resources:
                      requests:
                        cpu: 0.5
                        memory: 800Mi
                    ports:
                      - containerPort: 27017
                    volumeMounts:
                      - name: mongo-ebs-claim
                        mountPath: /data/db
                    env:
                      - name: SOME_API_KEY
                        valueFrom:
                          secretKeyRef:
                            name: "{{ var_secret_name }}"
                            key: apikey
                volumes:
                  - name: appconfig
                    configMap:
                      name: appconfigmap
            volumeClaimTemplates:
              - metadata:
                  name: mongo-ebs-claim
                spec:
                  accessModes: [ "ReadWriteOnce" ]
                  resources:
                    requests:
                      storage: 5Gi
