## Assumptions
- did not have a spare EKS cluster and thought that would be helpful so I [spun one up](https://github.com/aws-samples/aws-amazon-eks-ansible-example/tree/main)
- the [diagram](https://github.com/aws-samples/aws-amazon-eks-ansible-example/tree/main#high-level-architecture) shows the rest of the architecture
- minimal: 2 workers
- outed out of DNS
- the playbook spins up a bastion to deploy the rest of the cluster, this made it a convenient gitlab runner for the deployment pipeline below

## Deployment pipeline
can be [found under Build](https://gitlab.com/ryanamorrison/kube-ansible-project/-/pipelines) and in the `.gitlab-ci.yml` file

## Resources Diagram 
is located in the [wiki](https://gitlab.com/ryanamorrison/kube-ansible-project/-/wikis/resources-diagram)
